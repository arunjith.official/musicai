const { getDefaultConfig } = require("expo/metro-config");

const defaultConfig = getDefaultConfig(__dirname);

module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
    babelTransformerPath: require.resolve("react-native-sass-transformer"),
  },
  resolver: {
    assetExts: [
      ...defaultConfig.resolver.assetExts.filter(
        (ext) => ext !== "scss" && ext !== "sass"
      ),
      "scss",
      "sass",
    ],
  },
};



