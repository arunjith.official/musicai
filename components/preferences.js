import React, { useState } from "react";
import { View, TouchableOpacity, Text } from "react-native";

const PreferenceButton = ({ title, onPress, selected }) => {
  const [opacity, setOpacity] = useState(1);

  const handlePress = () => {
    if (opacity === 1) {
      setOpacity(0.5);
    } else {
      setOpacity(1);
    }
    onPress();
  };

  return (
    <TouchableOpacity
      style={{
        backgroundColor: opacity === 0.5 ? "cyan" : "grey",
        padding: 10,
        opacity: opacity,
      }}
      onPress={handlePress}
      onPressOut={() => setOpacity(1)} // restore opacity when released
      activeOpacity={0.8} // set the opacity when pressed
    >
      <Text
        style={{
          color: "black",
          fontWeight: "bold",
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default PreferenceButton;
