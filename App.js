import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  Button,
  Settings,
} from "react-native";
import PreferenceButton from "./components/preferences";
import Appstyles from "./styles/style.scss";

console.log({Appstyles})

export default function App() {
  const [items, setItems] = useState([
    { name: "gaming", selected: false },
    { name: "pop", selected: false },
    { name: "lofi", selected: false },
    { name: "rage", selected: false },
    { name: "rap", selected: false },
    { name: "motivational", selected: false },
    { name: "punk", selected: false },
  ]);

  const [selectedOptions, setSelectedOptions] = useState([]);
  const [selectedCount, setSelectedCount] = useState(0);
  const [buttonClicked, setButtonClicked] = useState(false);
  const renderItem = ({ item }) => (
    <View style={Appstyles.item}>
      <Text>{item.name}</Text>
    </View>
  );

  const setSelectedPreferences = () => {
    const psudoItems = [...items];
    const selectedOptions = psudoItems
      .filter((item) => item.selected)
      .map((item) => item.name);
    const uniqueOptions = [...new Set(selectedOptions)];
    setSelectedOptions(uniqueOptions);
    setSelectedCount(uniqueOptions.length);
  };

  const canClickButton = () => {
    return selectedCount >= 3 && !buttonClicked;
  };

  const handleSelect = (index) => {
    const newItems = [...items];
    newItems[index].selected = !newItems[index].selected;
    setItems(newItems);
    setSelectedPreferences();
  };

  return (
    <View style={Appstyles.container}>
      <FlatList
        data={items}
        renderItem={({ item, index }) => (
          <PreferenceButton
            style={Appstyles.item}
            title={renderItem({ item })}
            onPress={() => handleSelect(index)}
            selected={renderItem({ item })}
          />
        )}
      />
      <Button
        style={Appstyles.button}
        title="Click me"
        disabled={!canClickButton()}
        onPress={() => (alert("Button clicked"), setButtonClicked(true))}
      />

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
  },
  item: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 70,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  button: {
    padding: 10,
    marginVertical: 40,
  },
});
